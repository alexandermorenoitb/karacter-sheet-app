package cat.itb.karactersheet.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "characters")
public class Character {
    @PrimaryKey (autoGenerate = true) int id;
    String name;
    int age;
    String origin;
    String gender;
    String past;
    String race;
    String attitude;
    String armas;
    String items;
    String virtue;

    public Character(){}

    public Character(String name, int age, String origin, String gender, String past, String race, String attitude, String armas, String items, String virtue, String currentWeight) {

        this.name = name;
        this.age = age;
        this.origin = origin;
        this.gender = gender;
        this.past = past;
        this.race = race;
        this.attitude = attitude;
        this.armas = armas;
        this.items = items;
        this.virtue = virtue;
    }

    public Character(int id) {
        this.id = id;
    }

    @Ignore


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPast() {
        return past;
    }

    public void setPast(String past) {
        this.past = past;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getAttitude() {
        return attitude;
    }

    public void setAttitude(String attitude) {
        this.attitude = attitude;
    }

    public String getArmas() {
        return armas;
    }

    public void setArmas(String armas) {
        this.armas = armas;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getVirtue() {
        return virtue;
    }

    public void setVirtue(String virtue) {
        this.virtue = virtue;
    }

}
