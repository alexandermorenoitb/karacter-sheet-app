package cat.itb.karactersheet.screens.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.karactersheet.R;
import cat.itb.karactersheet.model.Character;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder> {
    private List<Character> characters= new ArrayList<>();
    OnCharacterClickListener listener;

    public CharacterAdapter() {
    }




    public void setCharacters(List<Character> characters) {
        this.characters = characters;
        notifyDataSetChanged();
    }

    public void setListener(OnCharacterClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public CharacterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.character_item, parent, false);
        return new CharacterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterViewHolder holder, int position) {
        Character character = characters.get(position);
        holder.nombreItemTextView.setText(character.getName());
        holder.generoItemTextView.setText(character.getGender());
        holder.razaItemTextView.setText(character.getRace());
    }

    @Override
    public int getItemCount() {
        int size=0;
        if (characters!=null) size= characters.size();
        return size;
    }

    public class CharacterViewHolder extends  RecyclerView.ViewHolder{

        @BindView(R.id.nombreItemTextView)
        TextView nombreItemTextView;
        @BindView(R.id.generoItemTextView)
        TextView generoItemTextView;
        @BindView(R.id.razaItemTextView)
        TextView razaItemTextView;

        public CharacterViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.character_row)
        public void onCharacterViewClicked(){
            Character character = characters.get(getAdapterPosition());
            listener.onCharacterViewClicked(character);
        }
    }

    public interface OnCharacterClickListener {
        void onCharacterViewClicked (Character character);
    }

}
