package cat.itb.karactersheet.screens.startScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.karactersheet.R;
import cat.itb.karactersheet.model.Character;
import cat.itb.karactersheet.screens.adapter.CharacterAdapter;

public class StartScreen extends Fragment {

    @BindView(R.id.screenView_textName)
    EditText screenView_textName;
    @BindView(R.id.screenView_textAge)
    EditText screenView_textAge;
    @BindView(R.id.screenView_textGender)
    EditText screenView_textGender;
    @BindView(R.id.screenView_textArmas)
    EditText screenView_textArmas;
    @BindView(R.id.screenView_textPast)
    EditText screenView_textPast;
    @BindView(R.id.screenView_textOrigin)
    EditText screenView_textOrigin;
    @BindView(R.id.screenView_textAttitude)
    EditText screenView_textAttitude;
    @BindView(R.id.screenView_textItems)
    EditText screenView_textItems;
    @BindView(R.id.screenView_textVirtue)
    EditText screenView_textVirtue;
    @BindView(R.id.screenView_textRace)
    EditText screenView_textRace;

    int idCharacter;

    @BindView(R.id.recyclerviewListaCharacters)
    RecyclerView characterRecyclerView;
    private StartScreenViewModel mViewModel;
    CharacterAdapter characterAdapter;

    public static StartScreen newInstance() {
        return new StartScreen();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_screen_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        idCharacter = StartScreenArgs.fromBundle(getArguments()).getIdCharacter();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(StartScreenViewModel.class);

        characterRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        characterAdapter = new CharacterAdapter();
        characterRecyclerView.setLayoutManager(layoutManager);
        characterRecyclerView.setAdapter(characterAdapter);
        LiveData<List<Character>> characters = mViewModel.getCharacters();
        characters.observe(this,this::fillEditTextWithData);


    }

    private void fillEditTextWithData(List<Character> characters) {

        screenView_textName.setText(characters.get(1).getName());
        screenView_textAge.setText(characters.get(1).getAge());
        screenView_textPast.setText(characters.get(1).getPast());
        screenView_textArmas.setText(characters.get(1).getArmas());
        screenView_textAttitude.setText(characters.get(1).getAttitude());
        screenView_textItems.setText(characters.get(1).getItems());
        screenView_textOrigin.setText(characters.get(1).getOrigin());
        screenView_textRace.setText(characters.get(1).getRace());
        screenView_textVirtue.setText(characters.get(1).getVirtue());
        screenView_textGender.setText(characters.get(1).getGender());
    }



    @OnClick(R.id.btnAddCharacter)
    public void onViewClicked() {

        Character character = getCharacterAtributes();
        if (idCharacter != -1) {
            character.setId(idCharacter);
        }
        //el nav está mal
        mViewModel.saveCharacter(character);
        Navigation.findNavController(getView()).navigate(R.id.action_startScreen_to_characterEdition);//ESTO ESTÁ MAL!!!!! TIENE QUE LLEVAR A LA PANTALLA DE CREACIÓN DE PERSONAJES


    }

    private Character getCharacterAtributes() {
        /*

        String title = inputTitleExcursion.getEditableText().toString();

        String kmString = inputKm.getEditableText().toString();
        double km = 0;
        if (!kmString.isEmpty()) km = Double.valueOf(kmString);

        String hourString = inputHour.getEditableText().toString();
        double hour = 0;
        if (!hourString.isEmpty()) hour = Double.valueOf(hourString);


        float difficulty = ratingBar.getRating();

        String slopeString = inputSlope.getEditableText().toString();
        int slope = 0;
        if (!slopeString.isEmpty()) slope = Integer.valueOf(slopeString);

        String altitudeString = inputAltitude.getEditableText().toString();
        int altitude = 0;
        if (!altitudeString.isEmpty()) altitude = Integer.valueOf(altitudeString);

        boolean circular = checkBoxCircular.isChecked();
        String province = inputProvince.getEditableText().toString();
        String comarca = inputComarca.getEditableText().toString();
        String description = inputDesc.getEditableText().toString();
        String link = inputLink.getEditableText().toString();
        boolean adapted = adaptedCheckbox.isChecked();


        Character character = new Excursion(title, km, hour, difficulty, slope, altitude, circular, province, comarca, description, link, adapted);
        return character;

        */

        return new Character();//BORRAR!!!!

    }

}
