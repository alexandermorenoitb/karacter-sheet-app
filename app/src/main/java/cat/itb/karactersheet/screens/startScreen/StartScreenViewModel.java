package cat.itb.karactersheet.screens.startScreen;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import cat.itb.karactersheet.model.Character;
import cat.itb.karactersheet.repository.CharacterRepository;

public class StartScreenViewModel extends AndroidViewModel{

    CharacterRepository repository;

    public StartScreenViewModel(@NonNull Application application) {
        super(application);
         repository = new CharacterRepository(application);
     }



    public LiveData<List<Character>> getCharacters() {
        return repository.getCharacters();
    }

    public void saveCharacter(Character character) {

    }
}
