package cat.itb.karactersheet.screens.startScreen;

import android.os.Bundle;

import androidx.annotation.NonNull;

import java.util.HashMap;

class StartScreenArgs {
    private final HashMap arguments = new HashMap();

    private StartScreenArgs() {
    }

    private StartScreenArgs(HashMap argumentsMap) {
        this.arguments.putAll(argumentsMap);
    }

    @NonNull
    @SuppressWarnings("unchecked")
    public static StartScreenArgs fromBundle(@NonNull Bundle bundle) {
        StartScreenArgs __result = new StartScreenArgs();
        bundle.setClassLoader(StartScreenArgs.class.getClassLoader());
        if (bundle.containsKey("idCharacter")) {
            int idCharacter;
            idCharacter = bundle.getInt("idCharacter");
            __result.arguments.put("idCharacter", idCharacter);
        } else {
            throw new IllegalArgumentException("Required argument \"idCharacter\" is missing and does not have an android:defaultValue");
        }
        return __result;
    }

    @SuppressWarnings("unchecked")
    public int getIdCharacter() {
        return (int) arguments.get("idCharacter");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle toBundle() {
        Bundle __result = new Bundle();
        if (arguments.containsKey("idCharacter")) {
            int idCharacter = (int) arguments.get("idCharacter");
            __result.putInt("idCharacter", idCharacter);
        }
        return __result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        StartScreenArgs that = (StartScreenArgs) object;
        if (arguments.containsKey("idCharacter") != that.arguments.containsKey("idCharacter")) {
            return false;
        }
        if (getIdCharacter() != that.getIdCharacter()) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + getIdCharacter();
        return result;
    }

    @Override
    public String toString() {
        return "StartScreenArgs{"
                + "idCharacter=" + getIdCharacter()
                + "}";
    }

    public static class Builder {
        private final HashMap arguments = new HashMap();

        public Builder(StartScreenArgs original) {
            this.arguments.putAll(original.arguments);
        }

        public Builder(int idCharacter) {
            this.arguments.put("idCharacter", idCharacter);
        }

        @NonNull
        public StartScreenArgs build() {
            StartScreenArgs result = new StartScreenArgs(arguments);
            return result;
        }

        @NonNull
        public Builder setIdCharacter(int idCharacter) {
            this.arguments.put("idCharacter", idCharacter);
            return this;
        }

        @SuppressWarnings("unchecked")
        public int getIdCharacter() {
            return (int) arguments.get("idCharacter");
        }
    }
}
