package cat.itb.karactersheet.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;

import java.util.List;

import cat.itb.karactersheet.DAO.CharacterDAO;
import cat.itb.karactersheet.db.ApplicationDataBase;

public class CharacterRepository {
    CharacterDAO dao;

    public CharacterRepository(Context context) {
        ApplicationDataBase db= ApplicationDataBase.getINSTANCE(context);
        dao = db.getDAO();
    }

    public LiveData<List<Character>> getCharacters() {

        return dao.getCharacters();
        //
    }
}
