package cat.itb.karactersheet.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import java.util.List;

@Dao
public interface CharacterDAO {

    @Insert
    public void insert(Character excursion);

    @Query("SELECT * FROM characters")
    public LiveData<List<Character>> getCharacters();

    @Query("SELECT * FROM characters WHERE id = :id")
    public LiveData<Character> getCharactersById(int id);

    @Update
    void update(Character character);

    @Delete
    void delete(Character character);
}

